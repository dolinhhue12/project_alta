﻿namespace pro_alta02.Data.Entities
{
    public class LopHocTK
    {
        public string UserId { get; set; }
        public Usercustom Usercustom { get; set; }
        public int LopId { get; set; }
        public LopHoc LopHoc { get; set; }

    }
}
