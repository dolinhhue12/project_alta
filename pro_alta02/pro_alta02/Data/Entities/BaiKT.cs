﻿namespace pro_alta02.Data.Entities
{
    public class BaiKT
    {
        public int Id { get; set; }
        public int IdLoai { get; set; }
        public LoaiKT? LoaiKT { get; set; }
        public int IdUser { get; set; }
        public Usercustom? Usercustom { get; set; }
        public int IdMon { get; set; }
        public Mon? Mon { get; set; }
        public int ThoiGian { get; set; }
        public int TrangThai { get; set; }
        public int BaiLam { get; set; }
        public float Diem { get; set; }
    }
}
