﻿namespace pro_alta02.Data.Entities
{
    public class LoaiKT
    {
        public int Id { get; set; }
        public string? Ten { get; set; }
        public IEnumerable<BaiKT>? BaiKTs { get; set; }
        public IEnumerable<Lich>? Liches { get; set; }
    }
}
