﻿namespace pro_alta02.Data.Entities
{
    public class LoaiTL
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public IEnumerable<TaiLieu>? TaiLieus { get; set; }
    }
}
