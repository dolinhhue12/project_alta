﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace pro_alta02.Data.Entities
{
    public class ApplicationDBContext : IdentityDbContext<Usercustom>
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }
        public DbSet<BaiKT> BaiKTs {get; set;}
        public DbSet<Diem> Diems {get; set;}
        public DbSet<GiangDay> GiangDays {get; set;}
        public DbSet<KhoaHoc> KhoaHocs {get; set;}
        public DbSet<KhoaHocLopHoc> KhoaHocLopHocs {get; set;}
        public DbSet<Khoi> Khois {get; set;}
        public DbSet<Lich> Liches {get; set;}
        public DbSet<LoaiKT> LoaiKTs {get; set;}
        public DbSet<LoaiTL> LoaiTLs {get; set;}
        public DbSet<LopHoc> LopHocs {get; set;}
        public DbSet<LopHocTK> LopHocTKs {get; set;}
        public DbSet<Mon> Mons {get; set;}
        public DbSet<TaiLieu> TaiLieus {get; set;}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<LoaiTL>().HasMany<TaiLieu>(t => t.TaiLieus).WithOne(l => l.LoaiTL).HasForeignKey(l => l.IdLoai).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<LoaiKT>().HasMany<BaiKT>(t => t.BaiKTs).WithOne(l => l.LoaiKT).HasForeignKey(l => l.IdLoai).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Usercustom>().HasMany<GiangDay>(t => t.GiangDays).WithOne(l => l.Usercustom).HasForeignKey(l => l.UserId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<LopHocTK>().HasKey(LHTK => new { LHTK.LopId, LHTK.UserId });
            builder.Entity<KhoaHocLopHoc>().HasKey(LHTK => new { LHTK.IdLop, LHTK.IdKhoa });
            builder.Entity<Mon>().HasMany<KhoaHoc>(t => t.KhoaHocs).WithOne(l => l.Mon).HasForeignKey(l => l.IdMon).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Mon>()
                .HasOne<Diem>(u => u.Diem)
                .WithOne(c => c.Mon)
                .HasForeignKey<Diem>(c=>c.IdMon)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Khoi>().HasMany<LopHoc>(t => t.LopHocs).WithOne(l => l.Khoi).HasForeignKey(l => l.IdKhoi).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Usercustom>().HasMany<Lich>(t => t.Liches).WithOne(l => l.Usercustom).HasForeignKey(l => l.UserId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Khoi>().HasMany<Lich>(t => t.Liches).WithOne(l => l.Khoi).HasForeignKey(l => l.IdKhoi).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Mon>().HasMany<Lich>(t => t.Liches).WithOne(l => l.Mon).HasForeignKey(l => l.IdMon).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<LoaiKT>().HasMany<Lich>(t => t.Liches).WithOne(l => l.LoaiKT).HasForeignKey(l => l.IdLoaiKT).OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(builder);




        }
    }
       
}
