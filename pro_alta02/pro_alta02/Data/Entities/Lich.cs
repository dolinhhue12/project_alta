﻿namespace pro_alta02.Data.Entities
{
    public class Lich
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public Usercustom Usercustom { get; set; }
        public int IdMon { get; set; }
        public Mon Mon { get; set; }
        public int IdKhoi { get; set; }
        public Khoi Khoi { get; set; }
        public DateTime ThoiGian { get; set; }
        public int ThoiLuong { get; set; }
        public int IdLoaiKT { get; set; }
        public LoaiKT LoaiKT { get; set; }

    }
}
