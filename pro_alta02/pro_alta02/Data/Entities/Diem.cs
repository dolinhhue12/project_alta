﻿namespace pro_alta02.Data.Entities
{
    public class Diem
    {
        public int Id { get; set; }
        public int IdMon { get; set; }
        public Mon Mon { get; set; }
        public string UserId { get; set; }
        public Usercustom Usercustom { get; set; }
        public float ChuyenCan { get; set; }
        public float KTMieng { get; set; }
        public float HeSo2 { get; set; }
        public float HeSo3 { get; set; }
        public int KetQua { get; set; }
        public string DanhGia { get; set; }
        public DateTime NgayCapNhat { get; set; }
    }
}
