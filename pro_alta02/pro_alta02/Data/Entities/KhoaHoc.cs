﻿namespace pro_alta02.Data.Entities
{
    public class KhoaHoc
    {
        public int Id { get; set; }
        public int IdMon { get; set; }
        public Mon Mon { get; set; }
        public DateTime TGBatDau { get; set; }
        public DateTime TGKetThuc { get; set; }
        public int TrangThai { get; set; }
        public string LinkOnl { get; set; }
        public IEnumerable<KhoaHocLopHoc> khoaHocLopHocs { get; set; }
        

    }
}
