﻿namespace pro_alta02.Data.Entities
{
    public class TaiLieu
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public int SoTrang { get; set; }
        public string NoiDung { get; set; }
        public int IdLoai { get; set; }
        public LoaiTL? LoaiTL { get; set; }



    }
}
