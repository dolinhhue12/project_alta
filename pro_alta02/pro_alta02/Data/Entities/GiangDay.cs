﻿namespace pro_alta02.Data.Entities
{
    public class GiangDay
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public string UserId { get; set; }
        public Usercustom Usercustom { get; set; }
        public int SoTiet{ get; set; }
    }
}
