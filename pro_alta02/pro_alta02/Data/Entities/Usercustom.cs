﻿using Microsoft.AspNetCore.Identity;

namespace pro_alta02.Data.Entities
{
    public class Usercustom:IdentityUser
    {
        public string Name { get; set; }
        public DateTime DataOfBrith { get; set; }
        public IEnumerable<LopHocTK> LopHocTKs { get; set; }
        public IEnumerable<Diem> Diems { get; set; }
        public IEnumerable<Lich> Liches { get; set; }
        public IEnumerable<GiangDay> GiangDays { get; set; }

    }
}
