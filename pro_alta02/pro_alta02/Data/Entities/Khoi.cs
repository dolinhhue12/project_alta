﻿namespace pro_alta02.Data.Entities
{
    public class Khoi
    {
        public int Id { get; set; }
        public string? Ten { get; set; }
        public IEnumerable<LopHoc>? LopHocs { get; set; }
        public IEnumerable<Lich>? Liches { get; set; }
    }
}
