﻿namespace pro_alta02.Data.Entities
{
    public class KhoaHocLopHoc
    {
        public int IdLop { get; set; }
        public LopHoc LopHoc { get; set; }
        public int IdKhoa { get; set; }
        public KhoaHoc KhoaHoc { get; set; }
    }
}
