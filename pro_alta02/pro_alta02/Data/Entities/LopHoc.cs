﻿namespace pro_alta02.Data.Entities
{
    public class LopHoc
    {
        public int Id { get; set; }
        public string MaLop { get; set; }
        public int TrangThai { get; set; }
        public string ChuDe { get; set; }
        public int IdKhoi { get; set; }
        public Khoi Khoi { get; set; }
        public IEnumerable<LopHocTK> LopHocTKs { get; set; }
        public IEnumerable<KhoaHocLopHoc> KhoaHocLopHocs { get; set; }

    }
}
