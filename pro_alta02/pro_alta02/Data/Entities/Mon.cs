﻿namespace pro_alta02.Data.Entities
{
    public class Mon
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public Diem Diem { get; set; }
        public IEnumerable<Lich> Liches { get; set; }
        public IEnumerable<BaiKT> BaiKTs { get; set; }
        public IEnumerable<KhoaHoc> KhoaHocs { get; set; }
    }
}
