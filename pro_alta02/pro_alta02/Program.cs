using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;
using pro_alta02.Infrastructures.Repositories;
using pro_alta02.Services.BaiKTServices;
using pro_alta02.Services.DiemServices;
using pro_alta02.Services.GiangDayServices;
using pro_alta02.Services.KhoaHocServices;
using pro_alta02.Services.KhoiServices;
using pro_alta02.Services.LichServices;
using pro_alta02.Services.LoaiKTServices;
using pro_alta02.Services.LoaiTLServices;
using pro_alta02.Services.LopHocServices;
using pro_alta02.Services.MonServices;
using pro_alta02.Services.TaiLieuServices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddDbContext<ApplicationDBContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
    options.LogTo(Console.WriteLine);
});

builder.Services.AddIdentity<Usercustom, IdentityRole>(options =>
{
    options.Password.RequireDigit = false;
    options.Password.RequireLowercase = false; 
    options.Password.RequireUppercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.SignIn.RequireConfirmedAccount = false;


})
.AddEntityFrameworkStores<ApplicationDBContext>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<IBaiKTRepository, BaiKTRepository>();
builder.Services.AddTransient<IDiemRepository, DiemRepository>();
builder.Services.AddTransient<IGiangDayRepository, GiangDayRepository>();
builder.Services.AddTransient<IKhoaHocLopHocRepository, KhoaHocLopHocRepository>();
builder.Services.AddTransient<IKhoaHocRepository, KhoaHocRepository>();
builder.Services.AddTransient<IKhoiRepository, KhoiRepository>();
builder.Services.AddTransient<ILichRepository, LichRepository>();
builder.Services.AddTransient<ILoaiKTRepository, LoaiKTRepository>();
builder.Services.AddTransient<ILoaiTLRepository, LoaiTLRepository>();
builder.Services.AddTransient<ILopHocRepository, LopHocRepository>();
builder.Services.AddTransient<ILopHocTKRepository, LopHocTKRepository>();
builder.Services.AddTransient<IMonRepository, MonRepository>();
builder.Services.AddTransient<ITaiLieuRepository, TaiLieuRepository>();

builder.Services.AddTransient<IBaiKTService, BaiKTService>();
builder.Services.AddTransient<IDiemService, DiemService>();
builder.Services.AddTransient<IGiangDayService, GiangDayService>();
builder.Services.AddTransient<IKhoaHocService, KhoaHocService>();
builder.Services.AddTransient<IKhoiService, KhoiService>();
builder.Services.AddTransient<ILichService, LichService>();
builder.Services.AddTransient<ILoaiKTService, LoaiKTService>();
builder.Services.AddTransient<ILoaiTLService, LoaiTLService>();
builder.Services.AddTransient<ILopHocService, LopHocService>();
builder.Services.AddTransient<IMonService, MonService>();
builder.Services.AddTransient<ITaiLieuService, TaiLieuService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
