﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.TaiLieuServices
{
    public interface ITaiLieuService
    {
        IList<TaiLieu> GetAll();

        bool Create(TaiLieu entity);

        TaiLieu GetById(int id);

        bool Update(TaiLieu entity);

        bool Delete(int id);
    }
}