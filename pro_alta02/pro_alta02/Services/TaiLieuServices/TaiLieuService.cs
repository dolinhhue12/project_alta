﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Services.TaiLieuServices
{
    public class TaiLieuService : ITaiLieuService
    {
        private readonly ITaiLieuRepository taiLieuRepository;

        public TaiLieuService(ITaiLieuRepository taiLieuRepository)
        {
            this.taiLieuRepository = taiLieuRepository;
        }
        public bool Create(TaiLieu entity)
        {
            try
            {
                taiLieuRepository.Add(entity);
                taiLieuRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                taiLieuRepository.Delete(id);
                taiLieuRepository.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IList<TaiLieu> GetAll()
        {
            return taiLieuRepository.GetAll();
        }

        public TaiLieu GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(TaiLieu entity)
        {
            try
            {
                taiLieuRepository.Update(entity);
                taiLieuRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
