﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.KhoaHocServices
{
    public interface IKhoaHocService
    {
        IList<KhoaHoc> GetAll();

        bool Create(KhoaHoc entity);

        KhoaHoc GetById(int id);

        bool Update(KhoaHoc entity);

        bool Delete(int id);
    }
}