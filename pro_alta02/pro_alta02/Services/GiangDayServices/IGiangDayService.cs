﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.GiangDayServices
{
    public interface IGiangDayService
    {
        IList<GiangDay> GetAll();

        bool Create(GiangDay entity);

        GiangDay GetById(int id);

        bool Update(GiangDay entity);

        bool Delete(int id);
    }
}