﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.KhoiServices
{
    public interface IKhoiService
    {
        IList<Khoi> GetAll();

        bool Create(Khoi entity);

        Khoi GetById(int id);

        bool Update(Khoi entity);

        bool Delete(int id);
    }
}