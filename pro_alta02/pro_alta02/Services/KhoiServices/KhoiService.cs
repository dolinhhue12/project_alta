﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Services.KhoiServices
{
    public class KhoiService : IKhoiService
    {
        private readonly IKhoiRepository khoiRepository;

        public KhoiService(IKhoiRepository khoiRepository)
        {
            this.khoiRepository = khoiRepository;
        }
        public bool Create(Khoi entity)
        {
            try
            {
                khoiRepository.Add(entity);
                khoiRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                khoiRepository.Delete(id);
                khoiRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public IList<Khoi> GetAll()
        {
            return khoiRepository.GetAll();
        }

        public Khoi GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Khoi entity)
        {
            try
            {
                khoiRepository.Update(entity);
                khoiRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
