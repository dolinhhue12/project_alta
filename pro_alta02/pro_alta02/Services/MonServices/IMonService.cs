﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.MonServices
{
    public interface IMonService
    {
        IList<Mon> GetAll();

        bool Create(Mon entity);

        Mon GetById(int id);

        bool Update(Mon entity);

        bool Delete(int id);
    }
}