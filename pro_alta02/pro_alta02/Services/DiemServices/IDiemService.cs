﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.DiemServices
{
    public interface IDiemService
    {
        IList<Diem> GetAll();

        bool Create(Diem entity);

        Diem GetById(int id);

        bool Update(Diem entity);

        bool Delete(int id);
    }
}