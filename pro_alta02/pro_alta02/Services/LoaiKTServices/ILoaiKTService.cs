﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.LoaiKTServices
{
    public interface ILoaiKTService
    {
        IList<LoaiKT> GetAll();

        bool Create(LoaiKT entity);

        LoaiKT GetById(int id);

        bool Update(LoaiKT entity);

        bool Delete(int id);
    }
}