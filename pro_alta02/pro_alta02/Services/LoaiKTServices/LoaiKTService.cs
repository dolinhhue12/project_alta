﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;
using pro_alta02.Infrastructures.Repositories;

namespace pro_alta02.Services.LoaiKTServices
{
    public class LoaiKTService : ILoaiKTService
    {
        private readonly ILoaiKTRepository loaiKTRepository;

        public LoaiKTService(ILoaiKTRepository loaiKTRepository)
        {
            this.loaiKTRepository = loaiKTRepository;
        }
        public bool Create(LoaiKT entity)
        {
            try
            {
                loaiKTRepository.Add(entity);
                loaiKTRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                loaiKTRepository.Delete(id);
                loaiKTRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public IList<LoaiKT> GetAll()
        {
            return loaiKTRepository.GetAll();   
        }

        public LoaiKT GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(LoaiKT entity)
        {
            try
            {
                loaiKTRepository.Update(entity);
                loaiKTRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
