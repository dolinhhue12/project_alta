﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.LopHocServices
{
    public interface ILopHocService
    {
        IList<LopHoc> GetAll();

        bool Create(LopHoc entity);

        LopHoc GetById(int id);

        bool Update(LopHoc entity);

        bool Delete(int id);
    }
}