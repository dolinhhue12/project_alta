﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.BaiKTServices
{
    public interface IBaiKTService
    {
        IList<BaiKT> GetAll();

        bool Create(BaiKT entity);

        BaiKT GetById(int id);

        bool Update(BaiKT entity);

        bool Delete(int id);      
    }
}