﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.LoaiTLServices
{
    public interface ILoaiTLService
    {
        IList<LoaiTL> GetAll();

        bool Create(LoaiTL entity);

        LoaiTL GetById(int id);

        bool Update(LoaiTL entity);

        bool Delete(int id);
    }
}