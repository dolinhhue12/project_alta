﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Services.LoaiTLServices
{
    public class LoaiTLService : ILoaiTLService
    {
        private readonly ILoaiTLRepository loaiTLRepository;

        public LoaiTLService(ILoaiTLRepository loaiTLRepository)
        {
            this.loaiTLRepository = loaiTLRepository;
        }
        public bool Create(LoaiTL entity)
        {
            try
            {
                loaiTLRepository.Add(entity);
                loaiTLRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                loaiTLRepository.Delete(id);
                loaiTLRepository.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IList<LoaiTL> GetAll()
        {
            return loaiTLRepository.GetAll();
        }

        public LoaiTL GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(LoaiTL entity)
        {
            try
            {
                loaiTLRepository.Update(entity);
                loaiTLRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
