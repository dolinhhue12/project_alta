﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Services.LichServices
{
    public interface ILichService
    {
        IList<Lich> GetAll();

        bool Create(Lich entity);

        Lich GetById(int id);

        bool Update(Lich entity);

        bool Delete(int id);
    }
}