﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pro_alta02.Data.Entities;
using pro_alta02.Services.TaiLieuServices;

namespace pro_alta02.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TaiLieusController : ControllerBase
    {
        private readonly ITaiLieuService taiLieuService;

        public TaiLieusController(ITaiLieuService taiLieuService)
        {
            this.taiLieuService = taiLieuService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(taiLieuService.GetAll());
        }

        [HttpPost]
        public IActionResult Create(TaiLieu entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (taiLieuService.Create(entity))
                return Ok(entity);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(TaiLieu entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (taiLieuService.Update(entity))
                return Ok(entity);

            return BadRequest();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = taiLieuService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
