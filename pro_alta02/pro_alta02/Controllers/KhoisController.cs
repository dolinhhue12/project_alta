﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pro_alta02.Data.Entities;
using pro_alta02.Services.KhoiServices;

namespace pro_alta02.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class KhoisController : ControllerBase
    {
        private readonly IKhoiService khoiService;

        public KhoisController(IKhoiService khoiService)
        {
            this.khoiService = khoiService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(khoiService.GetAll());
        }

        [HttpPost]
        public IActionResult Create(Khoi entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (khoiService.Create(entity))
                return Ok(true);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(Khoi entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (khoiService.Update(entity))
                return Ok(true);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = khoiService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
