﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pro_alta02.Data.Entities;
using pro_alta02.Services.LoaiTLServices;

namespace pro_alta02.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoaiTLsController : ControllerBase
    {
        private readonly ILoaiTLService loaiTLService;

        public LoaiTLsController(ILoaiTLService loaiTLService)
        {
            this.loaiTLService = loaiTLService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(loaiTLService.GetAll());
        }

        [HttpPost]
        public IActionResult Create(LoaiTL entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if(loaiTLService.Create(entity))
                return Ok(entity);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(LoaiTL entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (loaiTLService.Update(entity))
                return Ok(entity);

            return BadRequest();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess =  loaiTLService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
