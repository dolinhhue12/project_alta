﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pro_alta02.Data.Entities;
using pro_alta02.Services.LoaiKTServices;

namespace pro_alta02.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoaiKTsController : ControllerBase
    {
        private readonly ILoaiKTService loaiKTService;

        public LoaiKTsController(ILoaiKTService loaiKTService)
        {
            this.loaiKTService = loaiKTService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(loaiKTService.GetAll());
        }

        [HttpPost]
        public IActionResult Create(LoaiKT entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (loaiKTService.Create(entity))
                return Ok(true);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(LoaiKT entity)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (loaiKTService.Update(entity))
                return Ok(true);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = loaiKTService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
