﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Infrastructures.IRepositories
{
    public interface IDiemRepository: IGenericRepository<Diem>
    {
    }
}
