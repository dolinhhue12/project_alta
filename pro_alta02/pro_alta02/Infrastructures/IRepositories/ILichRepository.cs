﻿using pro_alta02.Data.Entities;

namespace pro_alta02.Infrastructures.IRepositories
{
    public interface ILichRepository : IGenericRepository<Lich>
    {
    }
}
