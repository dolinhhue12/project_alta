﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace pro_alta02.Infrastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IList<TEntity> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);

        TEntity GetById(params object[] keyValues);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(params object[] keyValues);

        IList<TEntity> Find(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);

        public int SaveChanges();
    }
}
