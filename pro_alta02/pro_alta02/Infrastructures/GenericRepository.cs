﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using pro_alta02.Data.Entities;
using System.Linq.Expressions;

namespace pro_alta02.Infrastructures
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDBContext Context;

        protected DbSet<TEntity> DbSet;

        public GenericRepository(ApplicationDBContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
             DbSet.Add(entity);
        }

        public void Delete(params object[] keyValues)
        {
            var entityExisting = this.DbSet.Find(keyValues);
            if (entityExisting != null)
            {
                this.DbSet.Remove(entityExisting);
                return;
            }
            throw new ArgumentNullException($"{string.Join(";", keyValues)} was not found in the {typeof(TEntity)}");
        }

        public IList<TEntity> Find(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null)
        {
            IQueryable<TEntity> query = DbSet;
            if (includes != null)
                query = includes(query);
            return  query.Where(condition).ToList();
        }

        public IList<TEntity> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null)
        {
            IQueryable<TEntity> query = DbSet;

            if (includes != null)
            {
                query = includes(query);
            }

            if (orderBy != null)
                query = orderBy(query);

            return  query.ToList();
        }

        public TEntity GetById(params object[] keyValues)
        {
            return  DbSet.Find(keyValues);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
    }
}
