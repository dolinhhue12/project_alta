﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class BaiKTRepository : GenericRepository<BaiKT>, IBaiKTRepository
    {
        public BaiKTRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
