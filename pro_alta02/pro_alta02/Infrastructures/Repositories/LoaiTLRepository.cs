﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class LoaiTLRepository : GenericRepository<LoaiTL>, ILoaiTLRepository
    {
        public LoaiTLRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
