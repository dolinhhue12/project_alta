﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class LopHocTKRepository : GenericRepository<LopHocTK>, ILopHocTKRepository
    {
        public LopHocTKRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
