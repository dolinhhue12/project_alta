﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class LoaiKTRepository : GenericRepository<LoaiKT>, ILoaiKTRepository
    {
        public LoaiKTRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
