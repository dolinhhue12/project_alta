﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class LopHocRepository : GenericRepository<LopHoc>, ILopHocRepository
    {
        public LopHocRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
