﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class KhoaHocLopHocRepository : GenericRepository<KhoaHocLopHoc>, IKhoaHocLopHocRepository
    {
        public KhoaHocLopHocRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
