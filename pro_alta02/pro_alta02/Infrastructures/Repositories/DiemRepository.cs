﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class DiemRepository : GenericRepository<Diem>, IDiemRepository
    {
        public DiemRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
