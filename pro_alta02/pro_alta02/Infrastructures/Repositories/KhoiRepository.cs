﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class KhoiRepository : GenericRepository<Khoi>, IKhoiRepository
    {
        public KhoiRepository(ApplicationDBContext context): base(context)  
        {

        }
    }
}
