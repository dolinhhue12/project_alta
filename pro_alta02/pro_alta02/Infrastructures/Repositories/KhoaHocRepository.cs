﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class KhoaHocRepository : GenericRepository<KhoaHoc>, IKhoaHocRepository
    {
        public KhoaHocRepository(ApplicationDBContext context): base(context)   
        {

        }
    }
}
