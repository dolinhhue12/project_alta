﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class TaiLieuRepository : GenericRepository<TaiLieu>, ITaiLieuRepository
    {
        public TaiLieuRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
