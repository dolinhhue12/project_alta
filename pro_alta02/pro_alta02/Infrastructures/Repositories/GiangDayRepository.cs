﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class GiangDayRepository : GenericRepository<GiangDay>, IGiangDayRepository
    {
        public GiangDayRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
