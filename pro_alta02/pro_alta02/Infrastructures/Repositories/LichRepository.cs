﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class LichRepository : GenericRepository<Lich>, ILichRepository
    {
        public LichRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
