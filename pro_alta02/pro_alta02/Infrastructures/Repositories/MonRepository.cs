﻿using pro_alta02.Data.Entities;
using pro_alta02.Infrastructures.IRepositories;

namespace pro_alta02.Infrastructures.Repositories
{
    public class MonRepository : GenericRepository<Mon>, IMonRepository
    {
        public MonRepository(ApplicationDBContext context): base(context)
        {

        }
    }
}
